<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%user}}`.
 */
class m200824_142934_create_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%user}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(200),
            'login' => $this->string(50),
            'password' => $this->string('600')
        ]);

        $this->addForeignKey(
            'fk_game_user_id_gamer_one_id',
            'game',
            'gamer_one_id',
            'user',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk_game_user_id_gamer_two_id',
            'game',
            'gamer_two_id',
            'user',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%user}}');
        $this->dropForeignKey('fk_game_user_id_gamer_two_id', 'game');
        $this->dropForeignKey('fk_game_user_id_gamer_two_id', 'game');
    }
}
