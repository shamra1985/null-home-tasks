<?php

use yii\db\Migration;

/**
 * Class m200819_143757_add_border_to_rules_table
 */
class m200819_143757_add_border_to_rules_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(
            'rules',
            'is_border',
             $this->boolean(),
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn(
            'rules',
            'is_border',
        );
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200819_143757_add_border_to_rules_table cannot be reverted.\n";

        return false;
    }
    */
}
