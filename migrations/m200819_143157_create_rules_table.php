<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%rules}}`.
 */
class m200819_143157_create_rules_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%rules}}', [
            'id' => $this->primaryKey(),
            'dimention' => $this->integer()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%rules}}');
    }
}
