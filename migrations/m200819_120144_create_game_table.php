<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%game}}`.
 */
class m200819_120144_create_game_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%game}}', [
            'id' => $this->primaryKey(),
            'gamer_one_id' => $this->integer(),
            'gamer_two_id' => $this->integer(),
            'field_gamer_one_id' => $this->integer(),
            'field_gamer_two_id' => $this->integer(),
            'current_turn_gamer' => $this->integer(),
            'rules_id' => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%game}}');
    }
}
