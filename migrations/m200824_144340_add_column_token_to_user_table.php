<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%ken_to_user}}`.
 */
class m200824_144340_add_column_token_to_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('user', 'token', $this->string(200));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('user', 'token');
    }
}
