<?php

use yii\db\Migration;

/**
 * Class m200819_145724_create_fk_game_rules
 */
class m200819_145724_create_fk_game_rules extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addForeignKey(
            'fk_game_rules_id_rule_id',
            'game',
            'rules_id',
            'rules',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200819_145724_create_fk_game_rules cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200819_145724_create_fk_game_rules cannot be reverted.\n";

        return false;
    }
    */
}
