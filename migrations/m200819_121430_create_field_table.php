<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%field}}`.
 */
class m200819_121430_create_field_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%field}}', [
            'id' => $this->primaryKey(),
            'serialised_content' => $this->string(2000)
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%field}}');
    }
}
