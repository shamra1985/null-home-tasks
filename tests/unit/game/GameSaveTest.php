<?php namespace game;

use app\models\domain\Deck;
use app\models\domain\Field;
use app\models\Game;

class GameSaveTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;
    
    protected function _before()
    {
    }

    protected function _after()
    {
    }

    // tests
    public function testGameSave()
    {
        $input = [
            "1_1" => "поле_1",
            "2_1" => "поле_1",
            "1_10" => "поле_1",
            "1_9" => "поле_1"
        ];
        $mDeck=[];
        $eDecks = [];
        foreach ($input as $key=>$item)
        {
            $key_array = explode("_", $key);
            $d1 = new Deck();
            $d1->setCoordinate((int)$key_array[0],(int)$key_array[1]);
            $inputDeck[]  = $d1 ;
            $eDecks[] = $d1;

        }

        $myField=new Field();
        $myField->createSheeps($inputDeck);

        $eField = new Field();
        $eField->createSheeps($eDecks);

        $game = new Game(
            [
                'gamer_one_id' => 100,
                'gamer_two_id' => 101,
                'current_turn_gamer' => 100,
                'rules_id' => 1,
            ]
        );
        $game->setFields($myField, $eField);
        $game->save();
        $f = $game->getFirstField();

        expect($f)->isInstanceOf(Field::class);


    }
}