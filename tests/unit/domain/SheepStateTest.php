<?php namespace domain;

use app\models\domain\Deck;
use app\models\domain\Sheep;


class SheepStateTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;
    
    protected function _before()
    {
    }

    protected function _after()
    {
    }

    public function testHurt()
    {

        $sheep = new Sheep();
        $d1 = new Deck();
        $d2 = new Deck();
        $d3 = new Deck();

        $d1->setCoordinate(1,1);
        $d1->hit();
        $d2->setCoordinate(1,2);
        $d2->hit();
        $d3->setCoordinate(1,3);

        $sheep->setDecks([$d1,$d2,$d3]);

        expect($sheep->isAlive())->equals(false);
        expect($sheep->isDead())->equals(false);
        expect($sheep->isHurt())->equals(true);
    }

    // tests
    public function testAlive()
    {

        $sheep = new Sheep();
        $d1 = new Deck();
        $d2 = new Deck();
        $d3 = new Deck();

        $d1->setCoordinate(1,1);
        $d2->setCoordinate(1,2);
        $d3->setCoordinate(1,3);

        $sheep->setDecks([$d1,$d2,$d3]);

        expect($sheep->isAlive())->equals(true);
        expect($sheep->isDead())->equals(false);
        expect($sheep->isHurt())->equals(false);
    }

    // tests
    public function testDead()
    {

        $sheep = new Sheep();
        $d1 = new Deck();
        $d2 = new Deck();
        $d3 = new Deck();

        $d1->setCoordinate(1,1);
        $d1->hit();
        $d2->setCoordinate(1,2);
        $d2->hit();
        $d3->setCoordinate(1,3);
        $d3->hit();

        $sheep->setDecks([$d1,$d2,$d3]);

        expect($sheep->isAlive())->equals(false);
        expect($sheep->isDead())->equals(true);
        expect($sheep->isHurt())->equals(false);
    }

}