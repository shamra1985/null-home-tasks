<?php


namespace app\controllers;


use app\models\Game;
use app\models\User;
use yii\web\BadRequestHttpException;
use yii\web\Controller;

class PlayingController extends Controller
{

    /**
     * Отрисовываем два поля и два имени (пока так) и отправляем на метод start
     */
    public function actionIndex() {
       return $this->render('index');
    }

    /**
     * Получаем из формы имена игроков
     * создаем экземпляр класса Game
     * находим таких игроков в базе через User::findOne(['name'=>$nameOne]) берем их id и записываем в свойства игры
     * создаем два поля Field, наполняем их данными из $_POST
     *  запускаем по каждому полю проверку, если валидация не проходит говорим пользователю "Ай яй яй"
     *  помещаем оба поля в игру
     * берем один айдишник игрока и записываем в поле хода
     * сохраняем игру
     * редиректим пользователя на '?r=playing/show-field&id='.$id
     * @throws BadRequestHttpException
     */
    public function actionStart() {
        $nameOne = \Yii::$app->request->post('playerOne');
        $nameTwo = \Yii::$app->request->post('playerTwo');
        $decsOne = [];
        $decksTwo = [];
        foreach ( \Yii::$app->request->post() as $key => $postItem ){
            // todo хранить это надо в контроллере в константе, здесь только обращаться к этой константе
            if ($postItem == '1' && preg_match('/^[А-Я]_[0-9][0-9]?$/u', $key)){
                $decsOne[] = $key;
            }elseif($postItem == '2' && preg_match('/^[А-Я]_[0-9][0-9]?$/u', $key)) {
                $decksTwo[] = $key;
            }
        }
        $userOne = User::findOne(['name' => $nameOne]);
        if ($userOne == null){
            throw new BadRequestHttpException('Пользователя c именем '. $nameOne . ' не существует');
        }

        $game = new Game();
        $game->gamer_one_id = $userOne->id;
        $game->current_turn_gamer = $userOne->id;
//        $game->setFields();
//        $game->save();
        var_dump($userOne, $decksTwo, $decsOne);
//        return $this->redirect(['playing/show-field', ['id' => $game->id]]);
    }

    /**
     * находим игру с указанным айдишником (Game::findOne())
     * проверяем закончена ли игра, если закончена редиректим на метод '?r=playing/finish&game_id='.$id
     * узнаем чей сейчас ход (иделально сделать relation) и печатаем на экране "Ход игрока ".$currentGamer->name
     * потом рисуем поле, в котором будут отображены только подбитые палубы и выстрелы мимо
     * рисуем возможность выбрать только одну ячейку (option)
     * в качестве action в форме прописываем '?r=playing/hit&id=' и метод GET
     *
     *
     * @param int $id - айди игры, которую надо играть
     */
    public function actionPlay($id) {

    }

    /**
     * находим игру с указанным айдишником (Game::findOne())
     * узнаем чей сейчас ход (иделально сделать relation)
     * проверяем закончена ли игра, если закончена редиректим на метод '?r=playing/finish&game_id='.$id
     * достаем из игры поле противоположного игрока
     * стреляем по нему и получаем ответ
     * сохраняем поле через FieldStorage
     * если ответ мимо - то меняем текущего игрока и сохраняем изменную игру
     * если попал не меняем игрока
     * в любом случае редиректим опять на '?r=playing/play&id='.$game_id
     *
     * @param int $game_id - айди игры, которую надо играть
     */
    public function actionHit($game_id, $column, $row) {

    }

    /**
     * находим игру с указанным айдишником (Game::findOne())
     * проверяем на каком поле закончена игра
     * достаем из игры поле объект противоположного игрока игрока
     * пишем поздравляем выйграл такой-то чувак
     *
     * @param int $game_id - айди игры, которую надо играть
     */
    public function actionFinish($game_id) {

    }
}