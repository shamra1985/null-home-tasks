<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use app\models\domain\Deck;
use app\models\domain\Field;
use app\models\Game;
use app\models\Rules;
use app\models\User;
use yii\console\Controller;
use yii\console\ExitCode;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class HelloController extends Controller
{
    /**
     * This command echoes what you have entered as the message.
     * @param string $message the message to be echoed.
     * @return int Exit code
     */
    public function actionIndex($message = 'hello world')
    {

        $input = [
            "1_1" => "поле_1",
            "2_1" => "поле_1",
            "1_10" => "поле_1",
            "1_9" => "поле_1"
        ];
        $mDeck=[];
        $eDecks = [];
        foreach ($input as $key=>$item)
        {
            $key_array = explode("_", $key);
            $d1 = new Deck();
            $d1->setCoordinate((int)$key_array[0],(int)$key_array[1]);
            $inputDeck[]  = $d1 ;
            $eDecks[] = $d1;

        }

        $myField=new Field();
        $myField->createSheeps($inputDeck);

        $eField = new Field();
        $eField->createSheeps($eDecks);

        $u1 = new User([
            'name' => 'Лена',
            'login' => 'lena',
            'password' => '111',
            'token' => 'Token'
        ]);
        $u1->save();

        $u2 = new User([
            'name' => 'Даша',
            'login' => 'dasha',
            'password' => '111',
            'token' => 'Token'
        ]);
        $u2->save();

        $game = new Game(
            [
                'gamer_one_id' => $u1->id,
                'gamer_two_id' => $u2->id,
                'current_turn_gamer' => 100,
                'rules_id' => 1,
            ]
        );
        $game->setFields($myField, $eField);
        $game->save();
        $f = $game->getFirstField();
    }

    public function actionOne()
    {
        $g = Game::findOne(1);
        $f = $g->getFirstField();
        var_dump($f);

    }

    public function actionRulesTest()
    {
        $rules = new Rules([
            'dimention' =>  10,
            'is_border' => false,
        ]);
        $rules->save();
        var_dump($rules);
    }

    public function actionRulesOne()
    {
        $rules = Rules::findOne(1);
//        $rules->save();
        $rules->dimention = 13;
//        $rules->save();
        var_dump($rules);
    }

    public function actionGameRules()
    {
        $g = Game::findOne(1);
    }
}
