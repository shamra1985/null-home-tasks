<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Game */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="game-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'gamer_one_id')->textInput() ?>

    <?= $form->field($model, 'gamer_two_id')->textInput() ?>

    <?= $form->field($model, 'field_gamer_one_id')->textInput() ?>

    <?= $form->field($model, 'field_gamer_two_id')->textInput() ?>

    <?= $form->field($model, 'current_turn_gamer')->textInput() ?>

    <?= $form->field($model, 'rules_id')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
