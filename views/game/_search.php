<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\GameSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="game-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'gamer_one_id') ?>

    <?= $form->field($model, 'gamer_two_id') ?>

    <?= $form->field($model, 'field_gamer_one_id') ?>

    <?= $form->field($model, 'field_gamer_two_id') ?>

    <?php // echo $form->field($model, 'current_turn_gamer') ?>

    <?php // echo $form->field($model, 'rules_id') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
