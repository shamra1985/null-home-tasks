<?php


namespace app\models\domain;


class Deck
{
    /** @var bool $isAlive */
    protected $isAlive = true;
    /** @var int $column */
    protected $column;
    /** @var int $row */
    protected $row;

    /**
     * @return int
     */
    public function getColumn(): int
    {
        return $this->column;
    }

    /**
     * @return int
     */
    public function getRow(): int
    {
        return $this->row;
    }

    public function setCoordinate(int $column, int $row)
    {
        $this->column = $column;
        $this->row = $row;
    }

    public function isAlive() : bool
    {
        return $this->isAlive;
    }

    public function hit()
    {
        $this->isAlive = false;
    }
}