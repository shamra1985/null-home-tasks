<?php


namespace app\models\domain;


class Sheep
{
    /** @var Deck[] $decks */
    protected $decks;

    public function setDecks($decks)
    {
        $this->decks = $decks;
    }

    public function isAlive () : bool
    {
        foreach ($this->decks as $deck){
            if ( $deck->isAlive() == false)
            {
                return false;
            }
        }
        return true;
    }

    public function isDead () : bool
    {
        foreach ($this->decks as $deck){
            if ( $deck->isAlive() == true)
            {
                return false;
            }
        }
        return true;
    }

    public function isHurt () : bool
    {
        $hasAliveDecks = false;
        $hasDeadDecks = false;
        foreach ($this->decks as $deck){
            if ( $deck->isAlive() == true)
            {
                $hasAliveDecks = true;
            }else{
                $hasDeadDecks = true;
            }
        }
        return ($hasDeadDecks && $hasAliveDecks);
    }

    public function checkNearest(Deck $deck):bool
    {
        foreach ($this->decks as $existDeck)
        {
            if (
                $existDeck->getRow() == $deck->getRow()
                && abs($existDeck->getColumn() - $deck->getColumn()) == 1
            )
            {
                return true;

            }elseif (
                $existDeck->getColumn() == $deck->getColumn()
                && abs($existDeck->getRow() - $deck->getRow()) == 1
            )
            {
                return true;
            }
        }
        return false;
    }

    public function addDeck(Deck $deck)
    {
        $this->decks[]=$deck;
        //var_dump($this->decks);
    }

    public function hitInSheep($hit)
    {
        foreach ($this->decks as $deck) {
            if ($deck->getColumn() == $hit->getColumn()
                && $deck->getRow() == $hit->getRow()
            ) { /// палуба убита isAlive=false
                //echo Hit::KILL;
                $deck->hit();
                //return false;
            }
            /// палуба жива
        }
    }

}