<?php


namespace app\models\domain;


class Field
{

    /** @var Sheep[] $sheepArray */
    protected $sheepArray = [];

    public function setSheepArray($sheepArray)
    {
        $this->sheepArray = $sheepArray;
    }

    /**
     * @param Deck[] $deckArray
     */
    public function createSheeps(array $deckArray)
    {
        foreach ($deckArray as $deck)
        {
            $hasSheep = false;
            // проверяем нет ли такого корабля, чья это палуба
            foreach ($this->sheepArray as $sheep)
            {
                $isNeedToPush = false;
                $isNeedToPush = $sheep->checkNearest($deck);
                if ($isNeedToPush){
                    $sheep->addDeck($deck);
                    $hasSheep = true;
                }
            }
            // создаем кораблю из палубы
            if ($hasSheep == false) {
                $sheep = new Sheep();
                $sheep->setDecks([$deck]);
                $this->sheepArray[] = $sheep;
            }
        }
    }

    public function hitInFild($hit)
    {
        foreach ($this->sheepArray as $sheep)
        {
            $sheep->hitInSheep($hit);//отправили выстрел в корабль после чего опращиваем какой он
            //$sheep->isAlive();
            if ($sheep->isDead()||$sheep->isHurt())
           {
               Hit::KILL;
           }
            else
                Hit::MISS;        }
    }







/////////////////
    public function validateRules(Rulles $rules)
    { $inField=true;
    foreach ($this->sheepArray as $sheep)
        {
            foreach ($sheep->getMaxDeckCoordinateX() as $column=>$row)
            {
                 if ($column>$rules->getDimention()||$row>$rules->getDimention())
                {
                    $inField=false;
                }
                else $inField=true;
            }
        }
    return $inField;
    }


}