<?php


namespace domain;


class Hit
{    const KILL='Попал!';
     const MISS='Мимо!';

    /** @var int $column */
    protected $column;
    /** @var int $row */
    protected $row;

    /**
     * Hit constructor.
     * @param int $column
     * @param int $row
     */
    public function __construct(int $column, int $row)
    {
        $this->column = $column;
        $this->row = $row;
    }

    /**
     * @return int
     */
    public function getColumn(): int
    {
        return $this->column;
    }

    /**
     * @return int
     */
    public function getRow(): int
    {
        return $this->row;
    }




}