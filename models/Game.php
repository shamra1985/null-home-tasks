<?php

namespace app\models;

use app\models\domain\Field;
use Yii;

/**
 * This is the model class for table "game".
 *
 * @property int $id
 * @property int|null $gamer_one_id
 * @property int|null $gamer_two_id
 * @property int|null $field_gamer_one_id
 * @property int|null $field_gamer_two_id
 * @property int|null $current_turn_gamer
 * @property int|null $rules_id
 * @property Rules $rules
 * @property User $onePlayer
 * @property User $twoPlayer
 */
class Game extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'game';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['gamer_one_id', 'gamer_two_id', 'field_gamer_one_id', 'field_gamer_two_id', 'current_turn_gamer', 'rules_id'], 'default', 'value' => null],
            [['gamer_one_id', 'gamer_two_id', 'field_gamer_one_id', 'field_gamer_two_id', 'current_turn_gamer', 'rules_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'gamer_one_id' => 'Gamer One ID',
            'gamer_two_id' => 'Gamer Two ID',
            'field_gamer_one_id' => 'Field Gamer One ID',
            'field_gamer_two_id' => 'Field Gamer Two ID',
            'current_turn_gamer' => 'Current Turn Gamer',
            'rules_id' => 'Rules ID',
        ];
    }

    public function  extraFields()
    {
        return ['rules'];
    }

    public function setFields(Field  $fierstFeild, Field $secondField)
    {
        if ($this->field_gamer_one_id == null && $this->field_gamer_two_id == null){
            $one = new FieldStorage(['serialised_content' => urlencode(serialize($fierstFeild))]);
            $one->save();
            $this->field_gamer_one_id = $one->id;
            $two = new FieldStorage(['serialised_content' => urlencode(serialize($secondField))]);
            $two->save();
            $this->field_gamer_two_id = $two->id;
        }
    }

    public function getFirstField()
    {
        if ($this->field_gamer_one_id != null ){
            $field = FieldStorage::findOne($this->field_gamer_one_id);
            return unserialize(urldecode($field->serialised_content));
        }
    }

    public function getSecondField()
    {
        if ($this->field_gamer_one_id != null ){
            $field = FieldStorage::findOne($this->field_gamer_two_id);
            return unserialize(urldecode($field->serialised_content));
        }
    }


    /**
     * Gets query for [[Rules]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRules()
    {
        return $this->hasOne(Rules::class, ['id' => 'rules_id']);
    }

    public function getOnePlayer()
    {
        return $this->hasOne(User::class, ['id' => 'gamer_one_id' ]);
    }

    public function getTwoPlayer()
    {
        return $this->hasOne(User::class, ['id' => 'gamer_two_id' ]);
    }
}
