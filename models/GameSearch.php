<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Game;

/**
 * GameSearch represents the model behind the search form of `app\models\Game`.
 */
class GameSearch extends Game
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'gamer_one_id', 'gamer_two_id', 'field_gamer_one_id', 'field_gamer_two_id', 'current_turn_gamer', 'rules_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Game::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'gamer_one_id' => $this->gamer_one_id,
            'gamer_two_id' => $this->gamer_two_id,
            'field_gamer_one_id' => $this->field_gamer_one_id,
            'field_gamer_two_id' => $this->field_gamer_two_id,
            'current_turn_gamer' => $this->current_turn_gamer,
            'rules_id' => $this->rules_id,
        ]);

        return $dataProvider;
    }
}
